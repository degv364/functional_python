# Functional Python

A simple repo with Jupyter Notebooks for learning about functional style in Python

This notebook can be used on your computer, if you have [Jupyter](https://jupyter.org/)
installed. If not you can interact with it on [mybinder](https://mybinder.org/) by following
this [link](https://mybinder.org/v2/gl/degv364%2Ffunctional_python/master)

For watching the introduction notebook in presentation mode:

```
jupyter nbconvert intro.ipynb --to slides --post serve
```