from math import factorial


# Square Differences
def difference():
    return (sum(range(1, 101)))**2 - sum([x**2 for x in range(1, 101)])


# Approximation of e and pi
def e_step(n):
    return 1/factorial(n)


def pi_step(n):
    return 2**(n+1) * factorial(n)**2 / factorial(2*n+1)


def e_and_pi_approximations():
    return [
        sum([e_step(n) for n in range(1000)]),
        sum([pi_step(n) for n in range(1000)])
        ]


# Amicable numbers
def d(n):
    return sum([p for p in range(1, n) if n % p == 0])


def amicable(a, b):
    return a != b and d(a) == d(b)


def biggest():
    return max([(a, b) for a in range(1, 1000) for b in range(a)
                if amicable(a, b)])


# Digit 4th power
def digs(n):
    return map(int, str(n))


def result():
    return [a for a in range(1000, 10000)
            if a == sum(map(lambda x: x**4, digs(a)))]


def main():
    print("Difference: ", difference())
    print("Approximation of e: ", e_and_pi_approximations()[0])
    print("Approximation of pi: ", e_and_pi_approximations()[1])
    print("Biggest amicable pair: ", biggest())
    print("Numbers as sum of powers of digits: ", result())


if __name__ == "__main__":
    main()
