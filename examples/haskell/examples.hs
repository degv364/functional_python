module Main where
import Prelude hiding (pi) -- So that we don't have name conflicts

-- Square Differences
difference = sum [1..100] ^ 2 - sum [x ^ 2 | x <- [1..100]]

-- Approximation of e and pi
factorial 0 = 1
factorial n = product [1..n]

eStep n = 1 / factorial n
piStep n = 2**(n+1) * factorial n **2 / factorial (2*n+1)

e = sum [eStep n | n <- [0..1000]]
pi = sum [piStep n | n <- [0..85]]

-- Amicable Numbers
d n = sum [p | p <- [1..(n-1)], mod n p == 0]
amicable a b = a /= b && d a == d b
biggest =  foldl max (0,0) [(a,b) | a <- [1..1000], b <- [1..a], amicable a b]

-- Digit 4th powers
digs 0 = []
digs x = mod x 10 : digs (div x 10)

result = [a | a<-[1000..9999] , a == sum [dig^4 | dig <- digs a]]


-- Main Function
main = do
  print ("Difference: " ++ show difference)
  print ("Approximation of e: " ++ show e)
  print ("Approximation of pi: " ++ show pi)
  print ("Biggest amicable pair: " ++ show biggest)
  print ("Numbers as sum of powers of digits: " ++ show result)
