#include <numeric>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>
#include <functional>
// Some misc functions

// Syntax sugar for squaring.
// I want to avoid pow beacause it returns a double
template <typename Enumerable>
Enumerable square(Enumerable x){
  return x*x;
}

// This is a imperative Definition of an equivalent to the range python function
template <typename Number>
std::vector<Number> range(int init,int end){
  std::vector<Number> v(end-init);
  int index;
  for(index=init; index<end; index++){
    v[index-init] = (Number) index;
  }
  return v;
}

// Syntax sugar for sum function
template <typename Number>
Number sum(std::vector<Number> range){
  return std::accumulate(begin(range), end(range), (Number)0);
}

// Syntax sugar for map function
template <typename OValue, typename IValue, class UnaryOperation>
std::vector<OValue> map(UnaryOperation func, std::vector<IValue> iterable){
  std::vector<OValue> result(iterable.size());
  transform(iterable.begin(), iterable.end(), result.begin(), func);
  return result;
}

// Syntax sugar for filter function
template <typename T, class UnaryOperation>
std::vector<T> remove_if(UnaryOperation predicate, std::vector<T> iterable){
  std::vector<T> result = iterable;
  auto new_end = std::remove_if(result.begin(), result.end(), predicate);
  result.resize(new_end-result.begin());
  return result;
}

// Syntax sugar for adding an elemnt to vector
template<typename T>
std::vector<T> add_elem(T element, std::vector<T> iterable){
  iterable.push_back(element);
  return iterable;
}


// Printing vectors in a "Nice" way
template<typename T>
void vector_printer(std::vector<T> v){
  for (typename std::vector<T>::iterator it = v.begin(); it != v.end(); it++){
    std::cout<< *it << ",";
  }
  std::cout<<std::endl;
}


///*********************************************************************
// Square Differences
long int sum_squared(){
  return square<long>(sum<long>(range<long>(1,101)));
}

int square_sum(){
  // Here I am using c++ lambda abstractions
  return sum<long>(map<long>([] (long x)->long {return x*x;}
		       ,range<long>(1, 101)));
}

long int difference(){
  return sum_squared() - square_sum();
}

// Approximation of e and pi
double factorial(int n){
  std::vector<double> nums = range<double>(1, n+1);
  if (n==0) return 1;
  return std::accumulate(nums.begin(), nums.end(), 1.0,
			 std::multiplies<double>());
}

double e_step(int n){
  return 1.0 / factorial(n);
}

double pi_step(int n){
  return pow(2,n+1)*square<double>(factorial(n))/factorial(2*n+1);
}

double e(){
  return sum<double>(map<double>(e_step, range<double>(0, 101)));
}

double pi(){
  return sum<double>(map<double>(pi_step, range<double>(0, 80)));
}

// Amicable Numbers
int d(int n){
  return sum<int>(remove_if([n] (int m) {return n%m!=0;},
			    range<int>(1,n)));
}

bool amicable(int* a){
  return a[0] != a[1] && d(a[0]) == d(a[1]);
}

// Digit 4th powers
std::vector<int> digs(int n){
  if (n==0) return std::vector<int>(0);
  else return add_elem<int>(n%10, digs(n/10));
}

bool is_not_sum_4th_power(int n){
  return n != sum<int>(map<int>([](int x){return x*x*x*x;}, digs(n)));
}

std::vector<int> result(){
  return remove_if(is_not_sum_4th_power, range<int>(1000,10000));
}


int main(){
  std::cout<<"Difference: "<<difference()<<std::endl;
  std::cout<<"Approximation of e: "<<e()<<std::endl;
  std::cout<<"Approximation of pi: "<<pi()<<std::endl;
  std::cout<<"Biggest amicable pair: <Not implemented yet>"<<std::endl;
  std::cout<<"Numbers as sum of powers of digits: ";
  vector_printer(result());
  return 0;
}
