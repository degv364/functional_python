% Square Differences

difference = (sum(1:100))^2 - sum((1:100).^2)
  
% Approximation of e and pi
function r = e_step(n)
  r = 1 / factorial(n);
end

function r = pi_step(n)
  r = (2^(n+1) * factorial(n)^2) / factorial(2*n+1);
end

e = sum(arrayfun(@e_step, 0:1000))
pi = sum(arrayfun(@pi_step, 0:80))


% Amicable numbers

function r = d(n)
  r = sum(find(~mod(n, 1:(n-1))));
end

function b = amicable(x, y)
  b = ((x != y) && (d(x) == d(y)))
end

biggest = 'Not Implemented yet'

% Digit 4th powers
function r = digs(n)
  if n == 0
    r = [];
  else
    r = [digs(floor(n/10)), mod(n, 10)];
  end
end

function b = is_sum_4th_power(n)
  b = (n == sum(digs(n).^4));
end


result = (1000:10000)(arrayfun(@is_sum_4th_power, 1000:10000))



