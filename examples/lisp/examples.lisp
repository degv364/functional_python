;; Some previous syntax sugar
(defun range (min max)
  (loop for n from min below max collect n))

(defun sum (iterable)
  (reduce '+ iterable))

(defun max-tuple (x y)
  (if (and (> (first x) (first y)) (> (second x) (second y)))
      x
      y))

;; Square Differences
(defun square-sum ()
  (sum (map 'list (lambda (x) (expt x 2)) (range 1 101))))

(defun sum-squared ()
  (expt (sum (range 1 101)) 2))

(defvar difference (- (sum-squared) (square-sum)))

;; Approximation of e and pi
(defun factorial (n)
  (reduce '* (range 1 (+ n 1))))

(defun e-step (n)
  (/ 1 (factorial n)))

(defun pi-step (n)
  (/ (* (expt 2 (+ n 1))
	(expt (factorial n) 2))
     (factorial (+ (* 2 n) 1))))

;; This are stored as fractions of very big numbers
(defvar e (sum (map 'list #'e-step (range 0 1000))))
(defvar my-pi (sum (map 'list #'pi-step (range 0 1000))))

;; Amicable numbers

(defun d (n)
  (sum (remove-if-not
	(lambda (m) (= 0 (mod n m)))
	(range 1 n))))

(defun amicable (x)
  (and (/= (first x) (second x))
       (= (d (first x)) (d (second x)))))

(defvar biggest
  (reduce #'max-tuple
	  (remove-if-not #'amicable
			 (reduce #'append
				 (loop for a from 1 to 1000 collect
				      (loop for b from 1 to a collect
					   (list a b)))))))

;; Digit 4th power
(defun digs (n)
  (if (= n 0)
      nil
      (append (list (mod n 10)) (digs (/ (- n (mod n 10)) 10)))))

(defun sum-4th-digs (n)
  (sum (loop for a in (digs n) collect (expt a 4))))

(defvar result
  (remove-if-not
   (lambda (x) (= x (sum-4th-digs x)))
   (range 1000 9999)))


(format T "Difference: ~D~%" difference)
(format T "Approximation of e: ~D~%" (coerce e 'double-float))
(format T "Approximation of pi: ~D~%" (coerce my-pi 'double-float))
(format T "Biggest amicable pair: (~D, ~D)~%" (first biggest) (second biggest))
(format T "Numbers as sum of powers of digits: ~S~%" result)
